package tests;

import dataProviders.HomePageDP;
import org.testng.annotations.Test;
import pageObjects.HomePage;
import pageObjects.Languages;
import utils.SeleniumUtils;

import static org.testng.Assert.assertEquals;


public class LanguageTest extends BaseUITests{

    @Test(dataProvider = "changeLanguageDataProvider", dataProviderClass = HomePageDP.class)
    public void testLanguageChange(Languages language, String piese, String accesorii, String uleiuri){
        driver = SeleniumUtils.getDriver(browser);
        HomePage homePage = new HomePage(driver);
        homePage.openPage(hostname);

        homePage.changeLanguage(language);
        assertEquals(homePage.getPieseTabText(), piese);
        assertEquals(homePage.getAccesoriiTabText(), accesorii);
        assertEquals(homePage.getUleiuriTabText(), uleiuri);
        System.out.println("The used language is: " + language.toString());
    }

}
