package tests;

import objectModels.LoginModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.LoginPage;
import utils.SeleniumUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static org.testng.Assert.assertTrue;
import static utils.SeleniumUtils.getEscapedElement;


public class LoginTests extends BaseUITests {

    @DataProvider(name = "negativeLoginDP")
    public Iterator<Object[]> SQLDpCollection() {
        Collection<Object[]> dp = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://" + dbHostname + ":" + dbPort
                    + "/" + dbSchema, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM automation.negative_login;");
            while (resultSet.next()) {
                LoginModel lm = new LoginModel(getEscapedElement(resultSet, "username"),
                        getEscapedElement(resultSet, "password"),
                        getEscapedElement(resultSet, "loginErrorMessage"));
                dp.add(new Object[]{lm});
            }
        }catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp.iterator();
    }


    @Test(dataProvider = "negativeLoginDP")
    public void testNegativeloginWithDB(LoginModel lm) {
        driver = SeleniumUtils.getDriver(browser);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openPage(hostname);

        loginPage.login(lm.getAccount().getUsername(), lm.getAccount().getPassword());
        System.out.println("The user has logged in with email: " + lm.getAccount().getUsername() +
                " and password: " + lm.getAccount().getPassword());
        Assert.assertEquals(loginPage.getErrorMessage(), "Email sau parola gresita!");
        String expectedLoginErr = lm.getLoginError();
        System.out.println("Verify expected error : expected userError: " + expectedLoginErr);
    }


    @Test
    public void testPositiveLogin() {
        driver = SeleniumUtils.getDriver(browser);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openPage(hostname);

        loginPage.login(username2, password);
        assertTrue(loginPage.isReprezentantaMeaDisplayed(), "Is Reprezentanta Mea displayed?");
        System.out.println("The user has logged in with email: " + username2 + " and password: " + password);

        loginPage.logout();
        assertTrue(loginPage.isAuthentificationLinkDisplayed(), "Is Authentication link displayed?");
        System.out.println("The user has logged out!");
    }


}
