package tests;

import dataProviders.PiesePageDP;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.PiesePage;
import utils.SeleniumUtils;

import static org.junit.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class VinNumberTests extends BaseUITests {
    PiesePage piesePage;

    @BeforeMethod(alwaysRun = true)
    public void login(){
        driver = SeleniumUtils.getDriver(browser);
        piesePage = new PiesePage(driver);
        piesePage.openPage(hostname);
        piesePage.login(username7, password);
        assertTrue(piesePage.isReprezentantaMeaDisplayed());
        System.out.println("The user has logged in with email: " + username7 + " and password: " + password);
    }

    @Test(dataProvider = "searchArticleForVinNumberByReferenceDP", dataProviderClass = PiesePageDP.class)
    public void testSearchArticleByReference(String vinNumber, String car, String partsCategory, String subCategory, int quantity, String expectedArticle) {
        piesePage.searchVinNumber(vinNumber);
        assertTrue(piesePage.searchedCarModel(car).contains(car));
        System.out.println("You have searched for VIN number: " + vinNumber + ". This VIN number corresponds to the vehicle " + car);

        piesePage.searchArticleForVinNumberByReference(partsCategory, subCategory);
        assertTrue(piesePage.areResultsDisplayed());
        System.out.println("Results are displayed.");

        piesePage.addProductForVINToBasket();
        assertTrue(piesePage.isArticleAddedToBasketMessageDisplayed());
        System.out.println("Product from category: " + partsCategory + "-> " + subCategory + " added to the basket");

        piesePage.updateQuantityContinueToBasket(quantity);
        assertTrue(piesePage.isArticleInBasket(expectedArticle));
        piesePage.printBasketDetails();

        piesePage.deleteItem();
        assertTrue(piesePage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        piesePage.logout();
        assertTrue(piesePage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }


    @Test(dataProvider = "vinNumberSearchByImageDP", dataProviderClass = PiesePageDP.class)
    public void testSearchArticleByImage(String vinNumber, String car, String category, String subcategory, int quantity, String expectedArticle) {
        piesePage.searchVinNumber(vinNumber);
        assertTrue(piesePage.searchedCarModel(car).contains(car));
        System.out.println("You have searched for VIN number: " + vinNumber + ". This VIN number corresponds to the vehicle " + car);

        piesePage.searchArticleForVinNumberByImage(category, subcategory);
        assertTrue(piesePage.areResultsDisplayed());
        System.out.println("Results are displayed.");

        piesePage.addProductForVINToBasket();
        assertTrue(piesePage.isArticleAddedToBasketMessageDisplayed());
        System.out.println("Product from category: " + category + "->" + subcategory + " added to the basket");

        piesePage.updateQuantityContinueToBasket(quantity);
        assertTrue(piesePage.isArticleInBasket(expectedArticle));
        piesePage.printBasketDetails();

        piesePage.deleteItem();
        assertTrue(piesePage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        piesePage.logout();
        assertTrue(piesePage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }

}
