package tests;

import dataProviders.RegistrationDP;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.RegistrationPage;
import utils.SeleniumUtils;

import java.util.concurrent.TimeUnit;

public class RegistrationTests extends BaseUITests {

    @Test(dataProvider = "registrationDP", dataProviderClass = RegistrationDP.class)
    public void testNegativeRegistration(String email, String countryCode, String phone, String pas, String confirmPas, boolean hasCode,
                                         String codBardi, String pasBardi, String filiala, boolean abonare, boolean terms, boolean GDPR, String capthca,
                                         String phoneErr, String passErr, String confirmPassErr, String termsErr, String gdprErr, String captchaErr, String fieldsErr) {

        driver = SeleniumUtils.getDriver(browser);
        RegistrationPage registrationPage = new RegistrationPage(driver);
        registrationPage.openPage(hostname + "/regisztracio");

        registrationPage.registration(email, countryCode, phone, pas, confirmPas, hasCode, codBardi, pasBardi, filiala, abonare, terms, GDPR, capthca);
        Assert.assertEquals(registrationPage.getPhoneErrorText(), phoneErr);
        Assert.assertEquals(registrationPage.getPasswordErrorText(), passErr);
        Assert.assertEquals(registrationPage.getConfirmPassErrorText(), confirmPassErr);
        Assert.assertEquals(registrationPage.getTermsErrorText(), termsErr);
        Assert.assertEquals(registrationPage.getGdprErrorText(), gdprErr);
        Assert.assertEquals(registrationPage.getCaptchaErrorText(), captchaErr);
        Assert.assertEquals(registrationPage.getFieldsError(), fieldsErr);

        System.out.println("The bellow errors are registered:");
        registrationPage.printErrorMessages();

    }
}
