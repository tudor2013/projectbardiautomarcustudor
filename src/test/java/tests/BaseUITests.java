package tests;


import org.openqa.selenium.WebDriver;
import org.testng.IResultMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.internal.IResultListener;
import utils.SeleniumUtils;

import java.io.IOException;
import java.util.Properties;

import static org.testng.TestRunner.PriorityWeight.dependsOnMethods;

public class BaseUITests {
    WebDriver driver;
    String hostname;
    String username;
    String username2;
    String username3;
    String username4;
    String username5;
    String username6;
    String username7;
    String password;
    String browser;
    String dbHostname, dbUser, dbSchema, dbPassword, dbPort;

    @BeforeClass
    public void setUpBasics() throws IOException {
        //read values from config file
        Properties prop = SeleniumUtils.readProperties("src\\test\\resources\\framework.properties");

//    try to get browser from command line
        browser = System.getProperty("browser");
//      get default value
        if (browser == null)
            browser = prop.getProperty("browser");
        hostname = prop.getProperty("hostname");
        username = prop.getProperty("username");
        username2 = prop.getProperty("username2");
        username3 = prop.getProperty("username3");
        username4 = prop.getProperty("username4");
        username5 = prop.getProperty("username5");
        username6 = prop.getProperty("username6");
        username7 = prop.getProperty("username7");
        password = prop.getProperty("password");

        //get DB connection settings
        dbHostname = prop.getProperty("dbHostname");
        dbUser = prop.getProperty("dbUser");
        dbPort = prop.getProperty("dbPort");
        dbPassword = prop.getProperty("dbPassword");
        dbSchema = prop.getProperty("dbSchema");

//        driver = SeleniumUtils.getDriver(browser);
    }

    @AfterMethod
    public void saveScreenShotAtFailure(ITestResult result) {
        if (driver != null) {
            if (result.getStatus() == ITestResult.FAILURE) {
                //your screenshooting code goes here
                String testClass = result.getInstance().getClass().getSimpleName();
                String testName = result.getMethod().getMethodName();
                System.out.println("Take screen shoot..for " + testClass + "->" + testName);
                SeleniumUtils.takeScreenshot(driver, testClass, testName);
            } else {
                driver.quit();              //keeps the failed tests opened
            }
        }
    }

}
