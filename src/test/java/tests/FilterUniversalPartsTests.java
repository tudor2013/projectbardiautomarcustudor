package tests;

import dataProviders.UniversalPartsDP;
import org.junit.Assert;
import org.testng.annotations.Test;
import pageObjects.UniversalPartsPage;
import utils.SeleniumUtils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class FilterUniversalPartsTests extends BaseUITests{

    @Test(dataProvider = "filterOilsDB", dataProviderClass = UniversalPartsDP.class)
    public void testFilterOils(String type, String searchArticle, String size, String viscosity, String producer, String expectedProduct){
        driver = SeleniumUtils.getDriver(browser);
        UniversalPartsPage universalPartsPage = new UniversalPartsPage(driver, type);
        universalPartsPage.openPage(hostname);

        universalPartsPage.login(username, password);
        org.testng.Assert.assertTrue(universalPartsPage.isReprezentantaMeaDisplayed());
        System.out.println("The user has logged in with email: " + username + " and password: " + password);
        universalPartsPage.selectTypeOfArticle(type);
        System.out.println("Universal parts type: " + type + " selected");

        universalPartsPage.searchArticle(searchArticle);
        assertTrue(universalPartsPage.areResultsDisplayed());
        System.out.println("Results are displayed.");

        universalPartsPage.filterBySize(size);
        universalPartsPage. filterByViscosity(viscosity);
        universalPartsPage.filterResultsByProducer(producer);
        System.out.println("Results are filtered by size: " + size + ", viscosity: " + viscosity + " and producer.");

        universalPartsPage.addProductToBasket();
        assertTrue(universalPartsPage.isArticleAddedToBasketMessageDisplayed());
        assertTrue(universalPartsPage.productName().contains(expectedProduct));
        System.out.println("Article: " + expectedProduct + " added to the basket");

        universalPartsPage.continueToBasket();
        Assert.assertTrue(universalPartsPage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        universalPartsPage.deleteItem();
        assertTrue(universalPartsPage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");
    }
}
