package tests;

import dataProviders.PiesePageDP;
import org.junit.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.PiesePage;
import utils.SeleniumUtils;

import static org.junit.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static pageObjects.Vehicles.*;


public class SelectVehicleTests extends BaseUITests {
    PiesePage piesePage;

    @BeforeMethod(alwaysRun = true)
    public void login(){
        driver = SeleniumUtils.getDriver(browser);
        piesePage = new PiesePage(driver);
        piesePage.openPage(hostname);
        piesePage.login(username4, password);
        assertTrue(piesePage.isReprezentantaMeaDisplayed());
        System.out.println("The user has logged in with email: " + username4 + " and password: " + password);
    }


    @Test(dataProvider = "selectArticleForCarDP", dataProviderClass = PiesePageDP.class)
    public void testSelectArticleForCar(String producer, String model, String year, String engine, String category, String subCategory) {
        piesePage.selectVehicleType(AUTOTURISM);
        System.out.println("Vehicle type: " + AUTOTURISM.toString());

        piesePage.selectVehicleDetails(producer, model, year, engine);
        Assert.assertTrue(piesePage.isCategoryListForSelectedVehicleDisplayed());
        System.out.println("Producer: " + producer + ", model: " + model + ", made in year: " + year + ", engine: " + engine + " selected");

        piesePage.selectArticle(category, subCategory);
        piesePage.addProductToBasket();
        Assert.assertTrue(piesePage.isArticleAddedToBasketMessageDisplayed());
        System.out.println(subCategory + " was added to the basket");

        piesePage.continueToBasket();
        Assert.assertTrue(piesePage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        piesePage.deleteItem();
        Assert.assertTrue(piesePage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        piesePage.logout();
        assertTrue(piesePage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }


    @Test(dataProvider = "selectArticleForVanDP", dataProviderClass = PiesePageDP.class)
    public void testSelectArticleForVan(String producer, String model, String year, String engine, String category, String subCategory) {
        piesePage.selectVehicleType(UTILITARA);
        System.out.println("Vehicle type: " + UTILITARA.toString());

        piesePage.selectVehicleDetails(producer, model, year, engine);
        Assert.assertTrue(piesePage.isCategoryListForSelectedVehicleDisplayed());
        System.out.println("Producer: " + producer + ", model: " + model + ", made in year: " + year + ", engine: " + engine + " selected");

        piesePage.selectArticle(category, subCategory);
        piesePage.addProductToBasket();
        Assert.assertTrue(piesePage.isArticleAddedToBasketMessageDisplayed());
        System.out.println(subCategory + " was added to the basket");

        piesePage.continueToBasket();
        Assert.assertTrue(piesePage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        piesePage.deleteItem();
       Assert.assertTrue(piesePage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        piesePage.logout();
        assertTrue(piesePage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }

    @Test(dataProvider = "selectArticleForMotorcycleDP", dataProviderClass = PiesePageDP.class)
    public void testSelectArticleForMotorcycle(String producer, String model, String year, String engine, String category, String subCategory) {
        piesePage.selectVehicleType(MOTOCICLETA);
        System.out.println("Vehicle type: " + MOTOCICLETA.toString());

        piesePage.selectVehicleDetails(producer, model, year, engine);
        Assert.assertTrue(piesePage.isCategoryListForSelectedVehicleDisplayed());
        System.out.println("Producer: " + producer + ", model: " + model + ", made in year: " + year + ", engine: " + engine + " selected");

        piesePage.selectArticle(category, subCategory);
        piesePage.addProductToBasket();
        Assert.assertTrue(piesePage.isArticleAddedToBasketMessageDisplayed());
        System.out.println(subCategory + " was added to the basket");

        piesePage.continueToBasket();
        Assert.assertTrue(piesePage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        piesePage.deleteItem();
       Assert.assertTrue(piesePage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        piesePage.logout();
        assertTrue(piesePage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }

    @Test(dataProvider = "searchArticleForCarDP", dataProviderClass = PiesePageDP.class)
    public void testSearchArticleForCar(String producer, String model, String year, String engine, String searchedArticle) {
        piesePage.selectVehicleType(AUTOTURISM);
        System.out.println("Vehicle type: " + AUTOTURISM);

        piesePage.selectVehicleDetails(producer, model, year, engine);
        Assert.assertTrue(piesePage.isCategoryListForSelectedVehicleDisplayed());
        System.out.println("Producer: " + producer + ", model: " + model + ", made in year: " + year + ", engine: " + engine + " selected");

        piesePage.searchArticle(searchedArticle);
        piesePage.addProductToBasket();
        Assert.assertTrue(piesePage.isArticleAddedToBasketMessageDisplayed());
        System.out.println(searchedArticle + " was added to the basket");

        piesePage.continueToBasket();
        Assert.assertTrue(piesePage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        piesePage.deleteItem();
        Assert.assertTrue(piesePage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        piesePage.logout();
        assertTrue(piesePage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }

    @Test(dataProvider = "searchArticleForVanDP", dataProviderClass = PiesePageDP.class)
    public void testSearchArticleForVan(String producer, String model, String year, String engine, String searchedArticle) {
        piesePage.selectVehicleType(UTILITARA);
        System.out.println("Vehicle type: " + UTILITARA.toString());

        piesePage.selectVehicleDetails(producer, model, year, engine);
        Assert.assertTrue(piesePage.isCategoryListForSelectedVehicleDisplayed());
        System.out.println("Producer: " + producer + ", model: " + model + ", made in year: " + year + ", engine: " + engine + " selected");

        piesePage.searchArticle(searchedArticle);
        piesePage.addProductToBasket();
        Assert.assertTrue(piesePage.isArticleAddedToBasketMessageDisplayed());
        System.out.println(searchedArticle + " was added to the basket");

        piesePage.continueToBasket();
        Assert.assertTrue(piesePage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        piesePage.deleteItem();
        Assert.assertTrue(piesePage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        piesePage.logout();
        assertTrue(piesePage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }

    @Test(dataProvider = "searchArticleForMotorcycleDP", dataProviderClass = PiesePageDP.class)
    public void testSearchArticleForMotorcycle(String producer, String model, String year, String engine, String searchedArticle) {
        piesePage.selectVehicleType(MOTOCICLETA);
        System.out.println("Vehicle type: " + MOTOCICLETA.toString());

        piesePage.selectVehicleDetails(producer, model, year, engine);
        Assert.assertTrue(piesePage.isCategoryListForSelectedVehicleDisplayed());
        System.out.println("Producer: " + producer + ", model: " + model + ", made in year: " + year + ", engine: " + engine + " selected");

        piesePage.searchArticle(searchedArticle);
        piesePage.addProductToBasket();
        Assert.assertTrue(piesePage.isArticleAddedToBasketMessageDisplayed());
        System.out.println(searchedArticle + " was added to the basket");

        piesePage.continueToBasket();
        Assert.assertTrue(piesePage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        piesePage.deleteItem();
        Assert.assertTrue(piesePage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        piesePage.logout();
        assertTrue(piesePage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }

}
