package tests;

import dataProviders.UniversalPartsDP;
import org.junit.Assert;
import org.testng.annotations.Test;
import pageObjects.UniversalPartsPage;
import utils.SeleniumUtils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class UniversalPartsTests extends BaseUITests{


    @Test(dataProvider = "selectArticleByImageDB", dataProviderClass = UniversalPartsDP.class)
    public void testSelectArticleByImage(String type, String category, String subCategory){
        driver = SeleniumUtils.getDriver(browser);
        UniversalPartsPage universalPartsPage = new UniversalPartsPage(driver, type);
        universalPartsPage.openPage(hostname);

        universalPartsPage.login(username5, password);
        org.testng.Assert.assertTrue(universalPartsPage.isReprezentantaMeaDisplayed());
        System.out.println("The user has logged in with email: " + username5 + " and password: " + password);
        universalPartsPage.selectTypeOfArticle(type);
        System.out.println("Universal parts type: " + type + " selected");

        universalPartsPage.selectByImage(category, subCategory);
        assertTrue(universalPartsPage.areResultsDisplayed());
        System.out.println("Results are displayed.");

        universalPartsPage.addProductToBasket();
        assertTrue(universalPartsPage.isArticleAddedToBasketMessageDisplayed());
        System.out.println("Product from category: " + category + "-> " + subCategory + " added to the basket");

        universalPartsPage.continueToBasket();
        Assert.assertTrue(universalPartsPage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        universalPartsPage.deleteItem();
        Assert.assertTrue(universalPartsPage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        universalPartsPage.logout();
        assertTrue(universalPartsPage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }


    @Test(dataProvider = "selectArticleByTextDB", dataProviderClass = UniversalPartsDP.class)
    public void testSelectArticleByText(String type, String category, String subCategory){
        driver = SeleniumUtils.getDriver(browser);
        UniversalPartsPage universalPartsPage = new UniversalPartsPage(driver, type);
        universalPartsPage.openPage(hostname);

        universalPartsPage.login(username6, password);
        org.testng.Assert.assertTrue(universalPartsPage.isReprezentantaMeaDisplayed());
        System.out.println("The user has logged in with email: " + username6 + " and password: " + password);
        universalPartsPage.selectTypeOfArticle(type);
        System.out.println("Universal parts type: " + type + " selected");

        universalPartsPage.selectByText(category, subCategory);
        assertTrue(universalPartsPage.areResultsDisplayed());
        System.out.println("Results are displayed.");

        universalPartsPage.addProductToBasket();
        assertTrue(universalPartsPage.isArticleAddedToBasketMessageDisplayed());
        System.out.println("Product from category: " + category + "-> " + subCategory + " added to the basket");

        universalPartsPage.continueToBasket();
        Assert.assertTrue(universalPartsPage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        universalPartsPage.deleteItem();
        Assert.assertTrue(universalPartsPage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        universalPartsPage.logout();
        assertTrue(universalPartsPage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }

    @Test(dataProvider = "searchArticleDB", dataProviderClass = UniversalPartsDP.class)
    public void testSearchArticleByText(String type, String searchArticle){
        driver = SeleniumUtils.getDriver(browser);
        UniversalPartsPage universalPartsPage = new UniversalPartsPage(driver, type);
        universalPartsPage.openPage(hostname);

        universalPartsPage.login(username6, password);
        assertTrue(universalPartsPage.isReprezentantaMeaDisplayed());
        System.out.println("The user has logged in with email: " + username6 + " and password: " + password);
        universalPartsPage.selectTypeOfArticle(type);
        System.out.println("Universal parts type: " + type + " selected");

        universalPartsPage.searchArticle(searchArticle);
        assertTrue(universalPartsPage.areResultsDisplayed());
        System.out.println("Results are displayed.");

        universalPartsPage.addProductToBasket();
        assertTrue(universalPartsPage.isArticleAddedToBasketMessageDisplayed());
        System.out.println("Article: " + searchArticle + " added to the basket");

        universalPartsPage.continueToBasket();
        Assert.assertTrue(universalPartsPage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        universalPartsPage.deleteItem();
        Assert.assertTrue(universalPartsPage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        universalPartsPage.logout();
        assertTrue(universalPartsPage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }
}
