package tests;

import com.opencsv.CSVReader;
import objectModels.HomepageModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.HomePage;
import utils.SeleniumUtils;

import java.io.File;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class SearchArticleFromHeaderTests extends BaseUITests{


    @DataProvider(name = "CSVDataProvider")
    public Object[][] csvDataProvider() throws Exception {
        File csvFile = new File("src\\test\\resources\\data\\testdata.csv");

        Reader reader = Files.newBufferedReader(Paths.get(csvFile.getAbsolutePath()));
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> csvData = csvReader.readAll();
        Object[][] dp = new Object[csvData.size()][1];
        int searchedArticlePoz = 0, expectedArticlePoz = 1;

        for (int i = 0; i < csvData.size(); i++) {
            HomepageModel lm = new HomepageModel(csvData.get(i)[searchedArticlePoz], csvData.get(i)[expectedArticlePoz]);
            dp[i] = new Object[]{lm};
        }
        return dp;
    }


    @Test(dataProvider = "CSVDataProvider")
    public void testSearchByText(HomepageModel hpm){
        driver = SeleniumUtils.getDriver(browser);
        HomePage homePage = new HomePage(driver);
        homePage.openPage(hostname);

        homePage.login(username2, password);
        Assert.assertTrue(homePage.isReprezentantaMeaDisplayed());
        System.out.println("The user has logged in with email: " + username2 + " and password: " + password);

        homePage.searchNameFromSearchBox(hpm.getSearchedArticle());
        assertTrue(homePage.areResultsDisplayed());
        System.out.println("Results are displayed.");

        homePage.addProductToBasketHome();
        assertTrue(homePage.isArticleAddedToBasketMessageDisplayed());
        Assert.assertTrue(homePage.productName().contains(hpm.getExpectedArticle()));
        System.out.println("Article: " + hpm.getSearchedArticle() + " added to the basket");


        homePage.continueToBasket();
        assertTrue(homePage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        homePage.deleteItem();
        assertTrue(homePage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        homePage.logout();
        assertTrue(homePage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }

}
