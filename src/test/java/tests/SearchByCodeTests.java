package tests;

import dataProviders.PiesePageDP;
import org.testng.annotations.Test;
import pageObjects.PiesePage;
import utils.SeleniumUtils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class SearchByCodeTests extends BaseUITests{


    @Test(dataProvider = "searchByCodeDP", dataProviderClass = PiesePageDP.class)
    public void testSearchArticleByCode(String article, int quantity, String expectedArticle){
        driver = SeleniumUtils.getDriver(browser);
        PiesePage piesePage = new PiesePage(driver);
        piesePage.openPage(hostname);

        piesePage.login(username3, password);
        assertTrue(piesePage.isReprezentantaMeaDisplayed());
        System.out.println("The user has logged in with email: " + username3 + " and password: " + password);

        piesePage.searchArticleByCode(article);
        assertTrue(piesePage.isArticleAddedToBasketMessageDisplayed());
        System.out.println("The article " + article + " was searched");

        piesePage.updateQuantityContinueToBasket(quantity);
        assertTrue(piesePage.isArticleInBasket(expectedArticle));
        piesePage.printBasketDetails();

        piesePage.deleteItem();
        assertTrue(piesePage.isTheBasketEmtpy());
        System.out.println("The article was deleted.");

        piesePage.logout();
        assertTrue(piesePage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }
}
