package tests;

import dataProviders.BasketPageDP;
import dataProviders.UniversalPartsDP;
import org.junit.Assert;
import org.testng.annotations.Test;
import pageObjects.HomePage;
import utils.SeleniumUtils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BasketTests extends BaseUITests{

    @Test(dataProvider = "basketPageDB", dataProviderClass = BasketPageDP.class)
    public void addMultipleProductsToBasketDeleteAll(String firstProduct, String secondProduct){
        driver = SeleniumUtils.getDriver(browser);
        HomePage homePage = new HomePage(driver);
        homePage.openPage(hostname);

        homePage.login(username, password);
        assertTrue(homePage.isReprezentantaMeaDisplayed());
        System.out.println("The user has logged in with email: " + username + " and password: " + password);

        homePage.searchNameFromSearchBox(firstProduct);
        assertTrue(homePage.areResultsDisplayed());
        System.out.println("Results are displayed.");

        homePage.addProductToBasketHome();
        assertTrue(homePage.isArticleAddedToBasketMessageDisplayed());
        System.out.println("Article added to the basket");

        homePage.continueShopping();
        System.out.println("Returned to the previous category list");

        homePage.searchNameFromSearchBox(secondProduct);
        assertTrue(homePage.areResultsDisplayed());
        System.out.println("Results are displayed.");

        homePage.addProductToBasketHome();
        assertTrue(homePage.isArticleAddedToBasketMessageDisplayed());
        System.out.println("Article added to the basket");

        homePage.continueToBasket();
        assertTrue(homePage.areThereArticlesInBasket());
        System.out.println("Shopping cart is opened");

        homePage.deleteAllFromBasket();
        assertTrue(homePage.isTheBasketEmtpy());
        System.out.println("The items from the basket were deleted");

        homePage.logout();
        Assert.assertTrue(homePage.isAuthentificationLinkDisplayed());
        System.out.println("The user has logged out!");
    }

}
