package utils;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

public class SeleniumUtils {
    public static final String SCREEN_SHOOT_PATH = "src\\test\\resources\\screenshots";
    static DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

    public static WebDriver getDriver(String browserType) {
        WebDriver driver = null;

        switch (Objects.requireNonNull(getBrowserEnumFromString(browserType))) {
            case CHROME:
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            case EDGE:
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver();
                break;
            case IE:
                WebDriverManager.iedriver().setup();
                driver = new InternetExplorerDriver();
                break;
        }

        return driver;
    }

    public static Browsers getBrowserEnumFromString(String browserType) {
        for (Browsers browser : Browsers.values()) {
            if (browserType.equalsIgnoreCase(browser.toString()))
                return browser;
        }
        System.out.println("Browser " + browserType + " is not on supported list");
        return null;
    }

    public static List<WebElement> waitForPresenceOfAllElements(WebDriver driver, long timeOut, By by) {
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        return wait.until(
                ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    //read from properties files
    public static Properties readProperties(String path) throws IOException {
        InputStream inputStream = new FileInputStream(path);
        Properties properties = new Properties();
        properties.load(inputStream);
        return properties;
    }

    public static WebElement waitForPresenceOfElement(WebDriver driver, long timeOut, By by) {
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        return wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public static WebElement waitForElementToBeClickable(WebDriver driver, long timeOut, By by) {
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        return wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public static void waitUntilIdentifiedElementIsVisibleAndClickeble(By by, WebDriver driver) {
        try {
            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(Duration.ofSeconds(10))
                    .pollingEvery(Duration.ofMillis(500))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(NullPointerException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(TimeoutException.class)
                    .ignoring(WebDriverException.class)
                    .ignoring(ElementClickInterceptedException.class);
            wait.until(ExpectedConditions.and(
                    ExpectedConditions.visibilityOfElementLocated(by),
                    ExpectedConditions.presenceOfElementLocated(by),
                    ExpectedConditions.elementToBeClickable(by)));
        } catch (WebDriverException e) {
            e.printStackTrace();
        }
    }

    public static WebElement waitUntilElementIsVisibleAndClickeble(By by, WebDriver driver) {
        try {
            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(Duration.ofSeconds(10))
                    .pollingEvery(Duration.ofMillis(500))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(NullPointerException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(TimeoutException.class)
                    .ignoring(WebDriverException.class)
                    .ignoring(ElementClickInterceptedException.class);
            wait.until(ExpectedConditions.and(
                    ExpectedConditions.visibilityOfElementLocated(by),
                    ExpectedConditions.presenceOfElementLocated(by),
                    ExpectedConditions.elementToBeClickable(by)));
            return wait.until(ExpectedConditions.elementToBeClickable(by));
        } catch (WebDriverException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void waitUntilListOfElementsIsVisible(By by, WebDriver driver) {
        try {
            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(Duration.ofSeconds(10))
                    .pollingEvery(Duration.ofMillis(500))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(NullPointerException.class)
                    .ignoring(TimeoutException.class);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
            wait.until((ExpectedConditions.visibilityOfAllElementsLocatedBy(by)));
        } catch (WebDriverException e) {
            e.printStackTrace();
        }
    }

    public static long waitForTextVisible(WebElement element, String inhalt, WebDriver driver) {
        long time = -System.currentTimeMillis();
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10, 125);
            wait.until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver d) {
                    return element.getAttribute("value").equals(inhalt);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        time = time + System.currentTimeMillis();
        return time;
    }

    public static WebElement webDriverWait(By by, WebDriver driver) {

        try {
            WebDriverWait wait = new WebDriverWait(driver, 10, 125);
            wait
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(ElementClickInterceptedException.class)
                    .until(new ExpectedCondition<Boolean>() {
                        public Boolean apply(WebDriver d) {
                            d.findElement(by);
                            return true;
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return waitForPresenceOfElement(driver, 10, by);
    }

    public static boolean retryingFindClick(WebDriver driver, By by) {
        boolean result = false;
        int attempts = 2;
        while (attempts < 15) {
            try {
                Objects.requireNonNull(waitUntilElementIsVisibleAndClickeble(by, driver)).click();
                result = true;
                break;
            } catch (StaleElementReferenceException | ElementClickInterceptedException e) {
                System.out.println("Attempt: " + attempts);
            }
            attempts++;
        }
        return result;
    }

    public void waitForByVisible(By by, WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 12, 125);
            /*return */
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //return null;
    }

    public static void takeScreenshot(WebDriver driver, String testClass, String testName) {
        //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        LocalDateTime currentDate = LocalDateTime.now();
        String dataTimeFormat = currentDate.format(formatter).split("\\.")[0].replaceAll(":", "");
        File screenShotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String screenShotName = SCREEN_SHOOT_PATH + "\\" + testClass + "-" + testName + "-" + dataTimeFormat + ".jpeg";
        File saveFile = new File(screenShotName);
        System.out.println("Save screen shot at path:" + screenShotName);
        try {
            FileUtils.copyFile(screenShotFile, saveFile);
        } catch (IOException e) {
            System.out.println("Cannot save screen shots. Please investigate");
        }
    }

    public static String getEscapedElement(ResultSet resultSet, String element) throws SQLException {
        return replaceElements(resultSet.getString(element), "''", "");
    }

    private static String replaceElements(String element, String valueToBeReplaced, String valueReplaceWith) {
        return element.replaceAll(valueToBeReplaced, valueReplaceWith);
    }


}
