package objectModels;

public class HomepageModel {
    private String searchedArticle;
    private String expectedArticle;

    public HomepageModel(String searchedArticle, String expectedArticle) {
        this.searchedArticle = searchedArticle;
        this.expectedArticle = expectedArticle;
    }

    public String getSearchedArticle() {
        return searchedArticle;
    }

    public void setSearchedArticle(String searchedArticle) {
        this.searchedArticle = searchedArticle;
    }

    public String getExpectedArticle() {
        return expectedArticle;
    }

    public void setExpectedArticle(String expectedArticle) {
        this.expectedArticle = expectedArticle;
    }
}
