package objectModels;


public class LoginModel {
    private AccountModel account;
    private String loginError;

    public LoginModel() {
    }

    public LoginModel(String username, String password, String loginError) {
        AccountModel ac = new AccountModel();
        ac.setUsername(username);
        ac.setPassword(password);

        this.account = ac;
        this.loginError = loginError;
    }

    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }

    public String getLoginError() {
        return loginError;
    }

    public void setLoginError(String loginError) {
        this.loginError = loginError;
    }

}
