package dataProviders;

import org.testng.annotations.DataProvider;


public class BasketPageDP {

    @DataProvider(name = "basketPageDB")
    public static Object[][] basketPageDB(){
        return new Object[][]{
                {"Antigel", "Filtru desecator"}
        };
    }
}
