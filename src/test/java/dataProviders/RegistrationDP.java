package dataProviders;

import org.testng.annotations.DataProvider;

public class RegistrationDP {

    @DataProvider(name = "registrationDP")
    public static Object[][] registrationDP(){
        return new Object[][]{
                {"", "40", "754677285", "Parola", "Bardiauto1", true, "Bardi", "Bardiauto1", "", true, true, true, "1234", "", "Vă rugăm alegeți o parola mai sigură.", "Confirmarea parolei nu este identică cu parola.", "", "", "Numerele introduse nu corespund celor afișate.", "Corectati campurile eronate!"},
                {"tudor@yahoo.com", "40", "", "Password1", "Password1", true, "Bardi", "Bardiauto1", "Ploiesti", true, true, true, "1234", "Precizați un număr de telefon valid!", "", "", "", "", "", ""},
                {"", "32", "754728", "Bardiauto1", "Bardiauto1", false, "", "", "Bucuresti", false, true, true, "4894", "Precizați un număr de telefon valid!", "", "", "", "", "", ""},
                {"tudor@yahoo.com", "40", "123456", "Mures11", "Mures", false, "", "", "", false, true, true, "", "Precizați un număr de telefon valid!", "Vă rugăm alegeți o parola mai sigură.", "Confirmarea parolei nu este identică cu parola.", "", "", "", ""},
                {"", "43", "754677285", "", "", false, "", "", "Mures", false, false, false, "", "Precizați un număr de telefon valid!", "Vă rugăm alegeți o parola mai sigură.", "", "", "", "", "" },
                {"tudor@yahoo.com", "40","754677285", "Bardiauto1", "Bardiauto1", true, "Bardi", "Bardiauto1", "Mures", true, true, true, "1234", "", "", "", "", "", "Numerele introduse nu corespund celor afișate.", "Corectati campurile eronate!"},
                {"tudor@yahoo.com", "40", "754677285", "Bard", "Bard", true, "Bardi", "Bardiauto1", "Arad", false, false, false, "", "", "Vă rugăm alegeți o parola mai sigură.", "", "Trebuie sa aceptati conditiile generale contractuale!", "Trebuie sa aceptati regulile despre protectia datelor!", "Precizati numarul de control", "Corectati campurile eronate!"},

        };
    }
}
