package dataProviders;

import org.testng.annotations.DataProvider;

public class UniversalPartsDP {

    @DataProvider(name = "selectArticleByImageDB")
    public static Object[][] selectArticleByImageDB() {
        return new Object[][]{
                {"Accesorii", "HIFI", "Antene"},
                {"Uleiuri", "Antigel", ""},
                {"DupaDimensiune", "Becuri", ""},
                {"Echipamente", "Scule cu acumulator", "Acumulatoare"},
        };
    }

    @DataProvider(name = "selectArticleByTextDB")
    public static Object[][] selectArticleByTextDB() {
        return new Object[][]{
                {"Accesorii", "HIFI", "Antene"},
                {"DupaDimensiune", "Acumulatoare", ""}
        };
    }

    @DataProvider(name = "filterOilsDB")
    public static Object[][] filterOilsDB() {
        return new Object[][]{
                {"Uleiuri", "Ulei pentru motor", "4 liter", "10W-40", "Castrol", "Magnatec 10W-40 A3/B4 4l"}
        };
    }

    @DataProvider(name = "searchArticleDB")
    public static Object[][] searchArticleDB() {
        return new Object[][]{
                {"Uleiuri", "Antigel"}
        };
    }

}
