package dataProviders;

import org.testng.annotations.DataProvider;

import static pageObjects.Languages.HUNGARIAN;
import static pageObjects.Languages.ROMANIAN;

public class HomePageDP {

    @DataProvider(name = "changeLanguageDataProvider")
    public static Object[][] changeLanguageDataProvider(){
        return new Object[][]{
                {HUNGARIAN, "Alkatrészek", "Felszerelési cikkek", "Olajok / Folyadékok"},
                {ROMANIAN, "Piese", "Accesorii", "Uleiuri / Lichide"}
        };
    }

}
