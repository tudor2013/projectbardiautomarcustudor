package dataProviders;

import org.testng.annotations.DataProvider;


public class PiesePageDP {

    @DataProvider(name = "searchByCodeDP")
    public static Object[][] searchByCodeDP(){
        return new Object[][]{
                {"OC47", 2, "Filtru ulei"},
                {"D4-684", 3, "Set reparare etrier"}
        };
    }

    @DataProvider(name = "selectArticleForCarDP")
    public static Object[][] selectArticleForCarDP(){
        return new Object[][]{
                {"opel", "astra", "2011", "1.7 - Diesel", "Sistem electric", "Indítókulcs"}
        };
    }

    @DataProvider(name = "selectArticleForVanDP")
    public static Object[][] selectArticleForVanDP(){
        return new Object[][]{
                {"iveco", "daily", "2014", "3.0 - Diesel", "Filtre", "Filtru de aer"}
        };
    }

    @DataProvider(name = "selectArticleForMotorcycleDP")
    public static Object[][] selectArticleForMotorcycleDP(){
        return new Object[][]{
                {"ducati", "panigale", "2017", "1199 - Benzina", "Sistem de frânare - ABS/ASR/ESP", "Placuţe de frână"}
        };
    }

    @DataProvider(name = "searchArticleForCarDP")
    public static Object[][] searchArticleForCarDP(){
        return new Object[][]{
                {"opel", "astra", "2011", "1.7 - Diesel", "Filtru de polen"}
        };
    }

    @DataProvider(name = "searchArticleForVanDP")
    public static Object[][] searchArticleForVanDP(){
        return new Object[][]{
                {"daftrucks", "105", "2018", "12.9 - Diesel", "Capac rezervor"}
        };
    }

    @DataProvider(name = "searchArticleForMotorcycleDP")
    public static Object[][] searchArticleForMotorcycleDP(){
        return new Object[][]{
                {"bmw", "rs", "2016", "1200 - Benzina", "Buji "}
        };
    }

    @DataProvider(name = "searchArticleForVinNumberByReferenceDP")
    public static Object[][] testSearchArticleForVinNumberByReferenceDP(){
        return new Object[][]{
                {"JMBLYV98W7J000460", "PAJERO", "Filters","Fuel Filter", 2, "Filtru carburant"}
        };
    }

    @DataProvider(name = "vinNumberSearchByImageDP")
    public static Object[][] vinNumberSearchByImageDP(){
        return new Object[][]{
                {"W0LPD6EG0BG056380", "ASTRA-J", "BRAKES", "FRONT BRAKE DISC", 2, "etrier cu pistonas"}
        };
    }


}
