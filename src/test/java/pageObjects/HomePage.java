package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.SeleniumUtils;

import java.util.List;

import static utils.SeleniumUtils.waitForPresenceOfAllElements;


public class HomePage extends BasketPage {

    @FindBy(css = "input#general_search_input")
    WebElement searchBox;

    @FindBy(css = ".mm_accessories>a")
    WebElement accesoriiTab;

    @FindBy(css = ".mm_oils>a")
    WebElement uleiuriTab;

    @FindBy(css = ".mm_bysize>a")
    WebElement dupaDimensiuneTab;

    @FindBy(css = ".mm_garage>a")
    WebElement echipamenteTab;

    @FindBy(xpath = "//a[contains(@data-original-title, 'Română limba utilizată')]")
    WebElement romanaLanguage;

    @FindBy(xpath = "//a[contains(@data-original-title, 'Maghiar limba utilizată')]")
    WebElement hungarianLanguage;

    @FindBy(css = "li.mm_carparts>a")
    WebElement pieseTab;

    By searchBoxBy = By.cssSelector("input#general_search_input");
    private final String addToBasketButton = "//i[@class = 'm-r-5 fa fa-cart-plus']";
    private final String displayedResults = "div.col-xs-12.p-5.medium-box-container.resp-medium-box";


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void changeLanguage(Languages language) {

        burgerMenu.click();
        switch (language) {
            case HUNGARIAN:
                System.out.println("Switch to hungarian language.");
                wait.until(ExpectedConditions.elementToBeClickable(hungarianLanguage));
                hungarianLanguage.click();
                break;
            case ROMANIAN:
                System.out.println("Switch to Romanian language.");
                wait.until(ExpectedConditions.elementToBeClickable(romanaLanguage));
                romanaLanguage.click();
                break;
        }
    }

    public String getPieseTabText() {
        return pieseTab.getText();
    }

    public String getAccesoriiTabText() {
        return accesoriiTab.getText();
    }

    public String getUleiuriTabText() {
        return uleiuriTab.getText();
    }

    public void searchNameFromSearchBox(String searchItem) {
            SeleniumUtils.waitUntilElementIsVisibleAndClickeble(searchBoxBy, driver);
            actions.moveToElement(searchBox).click();
            searchBox.clear();
            searchBox.sendKeys(searchItem);
        try {
            //the search is dynamic, wait for 2 seconds before results are displayed
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        selectFirstFromListWithActions("//a[contains(@title,'" + searchItem + "')]");
    }

    public void addProductToBasketHome() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div#searchModal")));
        addFirstProductToBasket(addToBasketButton);
    }

    public boolean areResultsDisplayed() {
        List<WebElement> results = waitForPresenceOfAllElements(driver, 30, By.cssSelector(displayedResults));
        return results.size() > 0;
    }


}

