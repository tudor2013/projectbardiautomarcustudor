package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import utils.SeleniumUtils;

import java.util.List;
import java.util.NoSuchElementException;

import static utils.SeleniumUtils.waitForElementToBeClickable;
import static utils.SeleniumUtils.waitForPresenceOfAllElements;

public class BasketPage extends AddToBasketWindow {

    @FindBy(xpath = "//div[@class = 'btn btn-default-danger btn-sm delete_item_button']")
    WebElement deleteItem;

    @FindBy(id = "confirm_ok")
    WebElement confirmDeleteShoppingCart;

    @FindBy(xpath = "//select[@name = 'quantity-select-box']")
    WebElement quantitySelect;

    @FindBy(css = "div#empty_cart_button")
    WebElement emptyCartButton;

    @FindBy(xpath = "//div[contains(text(), 'Coșul este gol')]")
    WebElement theBasketIsEmpty;

    By proceedToPaymentBy = By.cssSelector("button#next_step");
    By emptyCartButtonBy = By.cssSelector("div#empty_cart_button");
    By deleteItemnBy = By.xpath("//div[@class = 'btn btn-default-danger btn-sm delete_item_button']");
    By articlesDeletedMessageBy = By.xpath("//div[contains(text(), 'Stergere reusita.')]");
    By productsInBasketBy = By.xpath("//div[@class ='product-box-main-name text-color-darkgray']");


    public void deleteItem() {
        try {
            SeleniumUtils.waitUntilElementIsVisibleAndClickeble(deleteItemnBy, driver);
            deleteItem.click();
            waitForElementToBeClickableClick(confirmDeleteShoppingCart);
            wait.until(ExpectedConditions.refreshed(ExpectedConditions.presenceOfAllElementsLocatedBy(burgerMenuBy)));
        }catch (NoSuchElementException e){
            e.printStackTrace();
        }
    }

    public void updateQuantityFromTheBasket(int quantity) {
        Select quantityUpdate = new Select(quantitySelect);
        quantityUpdate.selectByValue(String.valueOf(quantity));
        wait.until(ExpectedConditions.elementToBeClickable(quantitySelect));
    }

    public void deleteAllFromBasket() {
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(emptyCartButtonBy, driver);
        emptyCartButton.click();
        waitForElementToBeClickableClick(confirmDeleteShoppingCart);
        //Thread.sleep needed because the alert message that confirms that the articles were deleted overlaps the menu, the alert can't be closed
        wait.until(ExpectedConditions.invisibilityOfElementLocated(articlesDeletedMessageBy));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.notify-message")));

    }

    public boolean areThereArticlesInBasket() {
        List<WebElement> productsInShoppingBasket = waitForPresenceOfAllElements(driver, 30, productsInBasketBy);
        return productsInShoppingBasket.size() > 0;
    }

    public void printBasketDetails() {
        List<WebElement> productsInShoppingBasket = waitForPresenceOfAllElements(driver, 30, productsInBasketBy);
        List<WebElement> priceOfProductsInShoppingBasket = waitForPresenceOfAllElements(driver, 30, By.xpath("//span[@class = 'text-right text-color-gray text-size-12 net-gross-text']/following-sibling::b"));
        System.out.println("Product in shopping basket: ");
        for (int i = 0; i < productsInShoppingBasket.size(); i++) {
            System.out.println("--->Product name: " + productsInShoppingBasket.get(i).getText() + " ,with total price: " + priceOfProductsInShoppingBasket.get(Math.round(i / 2)).getText());
        }
    }

    public boolean isTheBasketEmtpy() {
        wait.until(ExpectedConditions.elementToBeClickable(theBasketIsEmpty));
        return theBasketIsEmpty.isDisplayed();
    }

    public boolean isArticleInBasket(String article) {
        WebElement articleInBasket = waitForElementToBeClickable(driver, 30, By.xpath("//div[@class ='product-box-main-name text-color-darkgray' and contains(text(), '" + article + "')]"));
        return articleInBasket.isDisplayed();
    }

    public BasketPage(WebDriver driver) {
        super(driver);
    }
}
