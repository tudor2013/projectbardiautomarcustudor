package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.SeleniumUtils;

import java.util.NoSuchElementException;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//div[@class = 'cc-compliance']/a")
    WebElement acceptCookies;

    @FindBy(id = "pm_login_link")
    WebElement autentificare;

    @FindBy(xpath = "//input[@name= 'email']")
    WebElement emailAddress;

    @FindBy(xpath = "//input[@name= 'jelszo']")
    WebElement password;

    @FindBy(xpath = "//button[@type= 'submit']")
    WebElement submitButton;

    @FindBy(css = "li.pm_menu.padding")
    WebElement burgerMenu;

    @FindBy(css = "li.pm_logout>a")
    WebElement signOut;

    @FindBy(css = "div.bs-callout.bs-callout-danger")
    WebElement loginErrorMessage;

    @FindBy(xpath = "//span[@class = 'l-gray bold' and contains(text(), 'Reprezentanța mea:')]")
    WebElement reprezentantaMea;

    By autentificareBy = By.id("pm_login_link");
    By emailAddressBy = By.xpath("//input[@name= 'email']");
    By passwordBy = By.xpath("//input[@name= 'jelszo']");
    By burgerMenuBy = By.cssSelector("li.pm_menu.padding");
    By signOutBy = By.cssSelector("li.pm_logout>a");

    public void login(String email, String pass) {
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(autentificareBy, driver);
        autentificare.click();
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(emailAddressBy, driver);
        sendKeysToInputByWebElement(emailAddress, email);
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(passwordBy, driver);
        sendKeysToInputByWebElement(password, pass);
        submitButton.click();
    }

    public void logout() {
        wait.until(ExpectedConditions.refreshed(ExpectedConditions.presenceOfAllElementsLocatedBy(burgerMenuBy)));
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(burgerMenuBy, driver);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div#ajaxLoader")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.notify-message ")));
        burgerMenu.click();
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(signOutBy, driver);
        signOut.click();
    }



    public boolean isAuthentificationLinkDisplayed() {
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(autentificareBy, driver);
        return autentificare.isDisplayed();
    }

    //element is present only when the user is loged in
    public boolean isReprezentantaMeaDisplayed() {
        wait.until(ExpectedConditions.elementToBeClickable(reprezentantaMea));
        return reprezentantaMea.isDisplayed();
    }

    public String getErrorMessage() {
        return loginErrorMessage.getText();
    }

    public void openPage(String address) {
        driver.get(address);
        System.out.println("The page: " + driver.getCurrentUrl() + " was opened");
        String browserName = ((RemoteWebDriver) driver).getCapabilities().getBrowserName();
        System.out.println("The current test runs on the browser: " + browserName);
        //at the start of the test the layout of the left panel is broken, and can't be minimized =>problem solved with a refresh
        driver.navigate().refresh();
        acceptCookies.click();
        System.out.println("Accept cookies button was pressed");
    }

    public LoginPage(WebDriver driver) {
        super(driver);
    }
}
