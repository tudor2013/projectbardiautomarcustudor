package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.SeleniumUtils;

import static utils.SeleniumUtils.*;


public class UniversalPartsPage extends HomePage {
    @FindBy(css = "i.glyphicon.glyphicon-menu-hamburger")
    WebElement burgerMenu;

    @FindBy(xpath = "//div[@class = 'car-category-container tree-search']/input")
    WebElement searchInput;

    @FindBy(xpath = "//button[contains(text(), 'Filtrare')]")
    WebElement filtrareButton;

    private final String addToBasketButton = "//i[@class = 'm-r-5 fa fa-cart-plus']";
    private final String displayedResults = "div.col-xs-12.p-5.medium-box-container.resp-medium-box";
    By accesoriiTabBy = By.cssSelector(".mm_accessories>a");
    By uleiuriTabBy = By.cssSelector(".mm_oils>a");
    By dupaDimensiuneTabBy = By.cssSelector(".mm_bysize>a");
    By echipamenteTabBy = By.cssSelector(".mm_garage>a");
    By filtrareButtonBy = By.xpath("//button[contains(text(), 'Filtrare')]");

    public void selectTypeOfArticle(String type) {
        switch (type) {
            case "Accesorii":
                SeleniumUtils.waitUntilElementIsVisibleAndClickeble(accesoriiTabBy, driver);
                accesoriiTab.click();
                break;
            case "Uleiuri":
                SeleniumUtils.waitUntilElementIsVisibleAndClickeble(uleiuriTabBy, driver);
                uleiuriTab.click();
                break;
            case "DupaDimensiune":
                SeleniumUtils.waitUntilElementIsVisibleAndClickeble(dupaDimensiuneTabBy, driver);
                dupaDimensiuneTab.click();
                break;
            case "Echipamente":
                SeleniumUtils.waitUntilElementIsVisibleAndClickeble(echipamenteTabBy, driver);
                echipamenteTab.click();
                break;
        }
    }

    public void selectByImage(String category, String subCategory) {
        selectFirstFromList("//div[@class = 'category-name' and contains(text(), '" + category + "')]");
        if (!subCategory.isEmpty()) {
            selectFirstFromList("//div[@class = 'category-name' and contains(text(), '" + subCategory + "')]");
        }
    }

    public void openBurgerMenu() {
        wait.until(ExpectedConditions.elementToBeClickable(burgerMenu));
        burgerMenu.click();
        //if the list of articles doesn't load (remains in endless loading)
        while (!isElementPresentByWithWait(By.xpath("//ul[@class = 'tree-ul']/li"))) {
            driver.navigate().refresh();
            wait.until(ExpectedConditions.elementToBeClickable(burgerMenu));
            burgerMenu.click();
        }
    }

    public void selectByText(String category, String subCategory) {
        openBurgerMenu();
        makeSelection("//span[@class = 'highlight-text' and contains(text(), '" + category + "')]");
        if (!subCategory.isEmpty()) {
            makeSelection("//span[@class = 'highlight-text' and contains(text(), '" + subCategory + "')]");
        }
    }

    public void searchArticle(String searchArticle) {
        openBurgerMenu();
        sendKeysToInputByWebElement(searchInput, searchArticle);
        selectFirstFromList("//b[contains(text(), '" + searchArticle + "')]");
    }

    public void addProductToBasket() {
        SeleniumUtils.waitUntilListOfElementsIsVisible(By.cssSelector(displayedResults), driver);
        addFirstProductToBasket(addToBasketButton);
    }

    public void filterResultsBy(String filterName, String filterSelection) {
        SeleniumUtils.waitUntilListOfElementsIsVisible(By.cssSelector(displayedResults), driver);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.pn-loading")));
        try {
            SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath("//h3[contains(text(), '" + filterName + "')]"), driver).click();
        }catch (StaleElementReferenceException e){
            e.printStackTrace();
        }
        selectFirstFromList("//label[contains(text(), '" + filterSelection + "')]");
        waitUntilElementIsVisibleAndClickeble((filtrareButtonBy), driver);
        filtrareButton.click();
        SeleniumUtils.waitUntilListOfElementsIsVisible(By.cssSelector(displayedResults), driver);
    }

    public void filterResultsByProducer(String filterSelection) {
        filterResultsBy("Producator", filterSelection);
    }

    public void filterByViscosity(String filterSelection) {
        filterResultsBy("SAE", filterSelection);
    }

    public void filterBySize(String filterSelection) {
        filterResultsBy("Prezentare", filterSelection);
    }

    public UniversalPartsPage(WebDriver driver, String type) {
        super(driver);
    }
}
