package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.*;
import utils.SeleniumUtils;

import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static utils.SeleniumUtils.*;


public class AddToBasketWindow extends LoginPage {

    @FindBy(css = "h4#itemAddedToCartTitle")
    WebElement articleAddedToCartMessage;

    @FindBy(xpath = "//select[@name = 'quantity-select-box']")
    WebElement selectQuantityFromAddToBasketWindow;

    @FindBy(xpath = "//form[@id = 'basket_modal_form']/descendant::button[@class = 'close' and @aria-label = 'Închide']/span")
    WebElement closeWindowIcon;

    @FindBy(css = "a#itemAddedToCartCancel")
    WebElement continueShopping;

    @FindBy(css = "a#itemAddedToCartOk")
    WebElement proceedToBasketButton;

    By proceedToBasketButtonBy = By.cssSelector("a#itemAddedToCartOk");
    By continueShoppingBy = By.cssSelector("a#itemAddedToCartCancel");
    By selectQuantityFromAddToBasketWindowBy = By.xpath("//select[@name = 'quantity-select-box']");
    By articleAddedToCartMessageBy = By.cssSelector("h4#itemAddedToCartTitle");


    public void addFirstProductToBasket(String xpath) {
        SeleniumUtils.waitUntilListOfElementsIsVisible(By.xpath(xpath), driver);
        List<WebElement> resultsDisplayed = driver.findElements(By.xpath(xpath));
        WebElement firstMatchResult = resultsDisplayed.get(0);
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath(xpath), driver);
        firstMatchResult.click();
    }

    public boolean isArticleAddedToBasketMessageDisplayed() {
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(articleAddedToCartMessageBy, driver);
        return articleAddedToCartMessage.isDisplayed();
    }

    public void updateQuantityFromAddToBasketWindow(int quantity) {
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(selectQuantityFromAddToBasketWindowBy, driver);
        Select quantitySelect = new Select(selectQuantityFromAddToBasketWindow);
        quantitySelect.selectByValue(String.valueOf(quantity));
    }

    public void continueToBasket() {
        boolean invisible = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.modal-footer")));
        if (invisible) {
            try {
                //Objects.requireNonNull(waitUntilElementIsVisibleAndClickeble(proceedToBasketButtonBy, driver)).click();
                //webDriverWait(proceedToBasketButtonBy, driver).click();
                retryingFindClick(driver, proceedToBasketButtonBy);

            } catch (ElementClickInterceptedException | StaleElementReferenceException e) {

               // Objects.requireNonNull(waitUntilElementIsVisibleAndClickeble(proceedToBasketButtonBy, driver)).click();
                e.printStackTrace();
            }
        }
    }

    public void continueShopping() {
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(continueShoppingBy, driver);
        continueShopping.click();
    }


    public void updateQuantityContinueToBasket(int quantity) {
        updateQuantityFromAddToBasketWindow(quantity);
        continueToBasket();
    }


    public void updateQuantityContinueShopping(int quantity) {
        updateQuantityFromAddToBasketWindow(quantity);
        continueShopping();
    }

    public void closeAddtoBasketWindow() {
        closeWindowIcon.click();
    }


    public String productName() {
        WebElement productInfo = waitForPresenceOfElement(driver, 30, By.xpath("//p[@class = 'productinfo']/strong"));
        return productInfo.getText();
    }


    public AddToBasketWindow(WebDriver driver) {
        super(driver);
    }
}
