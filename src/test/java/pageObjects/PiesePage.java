package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.SeleniumUtils;

import java.util.List;
import java.util.Objects;

import static utils.SeleniumUtils.*;

public class PiesePage extends HomePage {

    @FindBy(xpath = "//div[@class = 'list-group']/a[@href= 'https://www.bardiauto.ro/webshop/keresok/alvazszam-kereso']")
    WebElement serieSasiu;

    @FindBy(css = "input#by-vim-input")
    WebElement vinInputSearch;

    @FindBy(xpath = "//h4[@class = 'icon-auto bardiicon']/parent::a")
    WebElement autoturism;

    @FindBy(xpath = "//h4[@class = 'icon-teher bardiicon']/parent::a")
    WebElement utilitara;

    @FindBy(xpath = "//h4[@class = 'icon-motor bardiicon']/parent::a")
    WebElement motocicleta;

    @FindBy(xpath = "//a[@data-href = '#dh-tab-part-numbers']")
    WebElement codReper;

    @FindBy(xpath = "//div[@id = 'by-vin-row-on-vin-search']/button")
    WebElement searchButton;

    @FindBy(css = "button.btn.btn-primary.choose.pull-right")
    WebElement alegeButton;

    private final String scrollToResultLocator = "//div[@id = 'simple-filter-props']/child::div[contains(@class, 'scroll_to_results')]";
    private final String scrollContainer = "#simple-filter-box-wrap";
    private final String productListByImage = "//button[@class = 'btn btn-primary' and not(contains(@style,'display: none')) and not(@id)]";
    private final String productListByReference = "//tbody/tr[@class = 'group-detail-row']/td[@class = 'text-right']/button";
    private final String searchResultsString = "//div[@class = 'col-xs-6 col-sm-6 col-md-4 col-lg-3 thumb-col']";
    private final String results = "div.product-box.p-5.text-left";
    private final String paths = "//button[contains(@class, 'btn-info btn btn-default')]";
    private final String addToBasketButton = "//i[@class = 'fa fa-cart-plus m-r-5']";
    private final String addToBasketButtonVIN = "//i[@class = 'm-r-5 fa fa-cart-plus']";
    private final String categoryList = "//li[@class = 'category_focus']/a";

    public PiesePage(WebDriver driver) {
        super(driver);
    }

    public void searchVinNumber(String vinNumber) {
        serieSasiu.click();
        vinInputSearch.sendKeys(vinNumber);
        searchButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(alegeButton));
        alegeButton.click();
    }

    public String searchedCarModel(String car) {
        WebElement searchedCarModel = waitForElementToBeClickable(driver, 30, By.xpath("//span[contains(text(), '" + car + "')]"));
        return searchedCarModel.getText();
    }

    public void searchArticleForVinNumberByReference(String partsCategory, String subCategory) {
        List<WebElement> searchPaths = waitForPresenceOfAllElements(driver, 30, By.xpath(paths));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.Aligner")));
        wait.until(ExpectedConditions.elementToBeClickable(searchPaths.get(1)));
        searchPaths.get(1).click();
        System.out.println("Lista repere is selected");
        WebElement parts = SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath("//span[text() = '" + partsCategory + "' and @class = 'original']/ancestor::a"), driver);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.Aligner")));
        parts.click();
        SeleniumUtils.waitUntilListOfElementsIsVisible(By.xpath("//ul[@class = 'tree-view length-28 menu-open main-tree']/li"), driver);
        WebElement subCat = SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath("//span[text() = '" + subCategory + "' and @class = 'original']/ancestor::a"), driver);
        actions.moveToElement(subCat).click().build().perform();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.Aligner")));
        List<WebElement> productsList = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(productListByReference)));
        productsList.get(0).click();
    }

    public void searchArticleForVinNumberByImage(String category, String subCategory) {
        //select category from the left category list
        SeleniumUtils.waitUntilListOfElementsIsVisible(By.cssSelector("ul.tree-view.menu-open.main-tree>li"), driver);
        WebElement selectedCategory = SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath("//span[text() = '" + category + "' and @class = 'original']/ancestor::a"), driver);
        selectedCategory.click();
        //select first corresponding image
        SeleniumUtils.waitUntilListOfElementsIsVisible(By.cssSelector("div.img-text-container"), driver);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.Aligner")));
        WebElement selectedSubCategory = SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath("//span[@class = 'original' and contains(text(), '" + subCategory + "')]/ancestor::div[@class = 'img-text-container']/div[@class = 'img-container']"), driver);
        selectedSubCategory.click();
        //select first coresponding product list
        selectFirstFromListWithActions(productListByImage);
    }

    public boolean areResultsDisplayed() {
        List<WebElement> searchResults = waitForPresenceOfAllElements(driver, 30, By.cssSelector(results));
        return searchResults.size() > 0;
    }

    public void addProductToBasket() {
//        SeleniumUtils.waitUntilListOfElementsIsVisible(By.cssSelector(results), driver);
        addFirstProductToBasket(addToBasketButton);
    }

    public void addProductForVINToBasket() {
        addFirstProductToBasket(addToBasketButtonVIN);
    }


    public void selectVehicleType(Vehicles vehicle) {
        switch (vehicle) {
            case AUTOTURISM:
                makeSelectionByWebElement(autoturism);
                break;
            case UTILITARA:
                makeSelectionByWebElement(utilitara);
                break;
            case MOTOCICLETA:
                makeSelectionByWebElement(motocicleta);
                break;
        }
    }

    public void selectDetails(String xpath) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ajax-progress-indicator   search-bar-indicator")));
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@class = 'list list-group']/descendant::a")));
        WebElement chooseCategory = waitForPresenceOfElement(driver, 15, By.xpath(xpath));
        actions.moveToElement(chooseCategory);
        Objects.requireNonNull(SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath(xpath), driver)).click();
    }

    public void selectVehicleDetails(String producer, String model, String year, String engine) {
        selectDetails("//input[@id = 'search_brands']/parent::div/descendant::a[@data-search-str='" + producer + "']");
        selectDetails("//input[@id = 'search_types']/parent::div/descendant::a[@data-search-str='" + model + "']");
        selectDetails("//input[@id = 'search_years']/parent::div/descendant::a[@data-search-str='" + year + "']");
        selectDetails("//input[@id = 'search_cm3fuel']/parent::div/descendant::a[@data-origstr ='" + engine + "']");
    }

    public boolean isCategoryListForSelectedVehicleDisplayed() {
        List<WebElement> categoryList = waitForPresenceOfAllElements(driver, 30, By.xpath("//li[@class = 'category_focus']/a"));
        return categoryList.get(0).isDisplayed();
    }

    public void selectArticle(String category, String subCategory) {
        SeleniumUtils.waitUntilListOfElementsIsVisible(By.xpath(categoryList), driver);
        makeSelection("//li[@class = 'category_focus']/a[text() = '" + category + "']");
        makeSelection("//li[@class = 'category_focus']/a[text() = '" + subCategory + "']");
        //for vehicles with multiple versions the add to basket button isn't visible until a scroll down is made
        clickWithWait(By.cssSelector(scrollContainer));
        clickWithWait(By.xpath(scrollToResultLocator));
    }

    public void searchArticle(String articleSearch) {
        SeleniumUtils.waitUntilListOfElementsIsVisible(By.xpath(categoryList), driver);
        sendKeysToInput("//input[@id = 'category_input_box']", articleSearch);
        if(isElementPresentByWithWait(By.xpath("//a[@class = 'focus']"))){
            WebElement focusedResultOfSearch = SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath("//a[@class = 'focus']"), driver);
            focusedResultOfSearch.click();
        } else  {
            sendKeysToInput("//input[@id = 'category_input_box']", articleSearch);
            WebElement focusedResultOfSearch = SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath("//a[@class = 'focus']"), driver);
            focusedResultOfSearch.click();
        }
        clickWithWait(By.cssSelector(scrollContainer));
        clickWithWait(By.xpath(scrollToResultLocator));
    }


    public void searchArticleByCode(String articleCode) {
        codReper.click();
        sendKeysToInput("//input[@id = 'part-number']", articleCode);
        //Select first available manufacturer code (Cod reper) or equivalent article (Echivalente)
        selectFirstFromList("//b[text() = '" + articleCode + "']");
        //wait for the list of available products loads
        List<WebElement> addToBasketIcons = waitForPresenceOfAllElements(driver, 30, By.xpath("//a[@class = 'cart-btn btn btn-primary']"));
        //if a manufacturer code is searched, article details is expanded
        if (isElementPresentByXpath("//a[@data-product-id = '" + articleCode + "' and @class = 'cart-btn btn btn-primary']")) {
            addFirstProductToBasket("//a[@data-product-id = '" + articleCode + "' and @class = 'cart-btn btn btn-primary']");
        } else {
            //For equivalent parts the user must select one of the displayed results
            selectFirstFromList("//td[@class = 'text-center search-result-list-retail-amount-td']/span/i[@class = 'ba undefined text-success ba-check-square ']");
            List<WebElement> resultsDisplayed = driver.findElements(By.xpath(addToBasketButtonVIN));
            WebElement firstMatchResult = resultsDisplayed.get(0);
            wait.until(ExpectedConditions.elementToBeClickable(firstMatchResult));
            firstMatchResult.click();
        }
    }


}
