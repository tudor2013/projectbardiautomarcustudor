package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static utils.SeleniumUtils.waitForPresenceOfAllElements;


public class RegistrationPage extends HomePage {

    @FindBy(xpath = "//input[@name = 'reg_email']")
    WebElement emailAddress;

    @FindBy(xpath = "//div[@class = 'selected-dial-code']")
    WebElement dialCode;

    @FindBy(xpath = "//input[@name ='mobilszam']")
    WebElement phoneNumber;

    @FindBy(xpath = "//input[@name = 'reg_jelszo']")
    WebElement password;

    @FindBy(xpath = "//input[@name='jelszoujra']")
    WebElement confirmPassword;

    @FindBy(xpath = "//label[contains(text(), 'Da, am')]/parent::div/div")
    WebElement hasClientCode;

    @FindBy(xpath = "//input[@name = 'bardikod']")
    WebElement bardiCode;

    @FindBy(xpath = "//input[@name = 'bardijelszo']")
    WebElement bardiPass;

    @FindBy(xpath = "//label[contains(text(), 'Nu am')]/parent::div/div")
    WebElement noClientCode;

    @FindBy(xpath = "//span[@class = 'select2-selection__rendered']")
    WebElement filialaLivrare;

    @FindBy(css = "input.select2-search__field")
    WebElement inputFiliala;

    @FindBy(xpath = "//div[@class ='col col-md-9']/label[contains(text(), 'Da')]")
    WebElement registerToNewsletter;

    @FindBy(xpath = "//input[@name = 'ba_aszf']/parent::div")
    WebElement acceptTerms;

    @FindBy(xpath = "//input[@name = 'ba_adatkezeles']/parent::div")
    WebElement acceptGDPR;

    @FindBy(id = "crs_reg_form_agree_yes")
    WebElement acceptGDPRFromWindow;

    @FindBy(id = "submit_reg")
    WebElement registerButton;

    @FindBy(xpath = "//div[@class = 'notify-message ' and contains(text(), 'Precizați un număr de telefon valid!')]")
    WebElement phoneError;

    @FindBy(xpath = "//div[@class = 'notify-message pass_notify' and contains(text(), 'Vă rugăm alegeți o parola mai sigură.')]")
    WebElement passError;

    @FindBy(xpath = "//div[@class = 'notify-message pass_notify' and contains(text(), 'Confirmarea parolei nu este identică cu parola.')]")
    WebElement confirmPassErr;

    @FindBy(xpath = "//div[@class = 'notify-message ' and contains(text(), 'Trebuie sa aceptati conditiile generale contractuale!')]")
    WebElement termsError;

    @FindBy(xpath = "//div[@class = 'notify-message ' and contains(text(), 'Trebuie sa aceptati regulile despre protectia datelor!')]")
    WebElement gdprError;

    @FindBy(xpath = "//div[@class = 'notify-message ' and contains(text(),'Numerele introduse nu corespund celor afișate.')]")
    WebElement captchaError;

    @FindBy(xpath = "//div[@class = 'notify-message ' and contains(text(),'Precizati numarul de control')]")
    WebElement captchaErrorEmpty;

    @FindBy(xpath = "//div[@class = 'notify-message ' and contains(text(), 'Corectati campurile eronate!')]")
    WebElement fieldsError;

    By phoneErrorBy = By.xpath("//div[@class = 'notify-message ' and contains(text(), 'Precizați un număr de telefon valid!')]");
    By passwordErrorBy = By.xpath("//div[@class = 'notify-message pass_notify' and contains(text(), 'Vă rugăm alegeți o parola mai sigură.')]");
    By confirmPassBy = By.xpath("//div[@class = 'notify-message pass_notify' and contains(text(), 'Confirmarea parolei nu este identică cu parola.')]");
    By termsErrorBy = By.xpath("//div[@class = 'notify-message ' and contains(text(), 'Trebuie sa aceptati conditiile generale contractuale!')]");
    By gdprErrorBy = By.xpath("//div[@class = 'notify-message ' and contains(text(), 'Trebuie sa aceptati regulile despre protectia datelor!')]");
    By captchErrorBy = By.xpath("//div[@class = 'notify-message ' and contains(text(),'Numerele introduse nu corespund celor afișate.')]");
    By captchErrorEmptyBy = By.xpath("//div[@class = 'notify-message ' and contains(text(),'Precizati numarul de control')]");
    By fieldsErrorBy = By.xpath("//div[@class = 'notify-message ' and contains(text(), 'Corectati campurile eronate!')]");


    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    public void registration(String email, String countryCode, String phone, String pas, String confirmPas, boolean hasCode,
                             String codBardi, String pasBardi, String filiala, boolean abonare, boolean terms, boolean GDPR, String capthca) {
        emailAddress.sendKeys(email);
        dialCode.click();
        List<WebElement> dialCodes = waitForPresenceOfAllElements(driver, 30, By.xpath("//li[@data-dial-code = '" + countryCode + "']"));
        WebElement dialcode = dialCodes.get(0);
        dialcode.click();
        phoneNumber.sendKeys(phone);
        password.sendKeys(pas);
        confirmPassword.sendKeys(confirmPas);

        filialaLivrare.click();
        sendKeysToInputByWebElementEnter(inputFiliala, filiala);
        if (hasCode) {
            hasClientCode.click();
            sendKeysToInputByWebElementEnter(bardiCode, codBardi);
            bardiPass.sendKeys(pasBardi);
        }
        if (abonare) {
            registerToNewsletter.click();
        }
        if (terms) {
            acceptTerms.click();
        }
        if (GDPR) {
            acceptGDPR.click();
            waitForElementToBeClickableClick(acceptGDPRFromWindow);
        }
        sendKeysToInput("//input[@name = 'captcha']", capthca);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
        waitForElementToBeClickableClick(registerButton);

    }

    public void printErrorMessages() {
        List<WebElement> errors = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("notify-message")));
        for (WebElement error : errors) {
            System.out.println(error.getText());
        }
    }

    public String getErrorText(WebElement webElement, By by) {
        if (isElementPresentBy(by)) {
            return webElement.getText();
        } else {
            return "";
        }
    }

    public String getPhoneErrorText() {
        return getErrorText(phoneError, phoneErrorBy);
    }

    public String getPasswordErrorText() {
        return getErrorText(passError, passwordErrorBy);
    }

    public String getConfirmPassErrorText() {
        return getErrorText(confirmPassErr, confirmPassBy);
    }

    public String getTermsErrorText() {
        return getErrorText(termsError, termsErrorBy);
    }

    public String getGdprErrorText() {
        return getErrorText(gdprError, gdprErrorBy);
    }

    public String getCaptchaErrorText() {
        if (isElementPresentBy(captchErrorBy)) {
            return captchaError.getText();
        } else if (isElementPresentBy(captchErrorEmptyBy)) {
            return captchaErrorEmpty.getText();
        } else
            return "";
    }

    public String getFieldsError() {
        return getErrorText(fieldsError, fieldsErrorBy);
    }
}
