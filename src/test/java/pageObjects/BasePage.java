package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.SeleniumUtils;

import java.util.List;

import static utils.SeleniumUtils.waitForElementToBeClickable;
import static utils.SeleniumUtils.waitForPresenceOfAllElements;

public class BasePage {
    WebDriver driver;
    WebDriverWait wait;
    Actions actions;

    public BasePage(WebDriver driver) {
        driver.manage().window().maximize();
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver, this);
        actions = new Actions(driver);
    }

    public void makeSelection(String xpath) {
        WebElement chooseCategory = SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath(xpath), driver);
        chooseCategory.click();
    }

    public void makeSelectionByWebElement(WebElement element) {
        waitForElementToBeClickableClick(element);
        System.out.println(element.getText() + " is selected");
    }

    public void selectFirstFromList(String xpath) {
        SeleniumUtils.waitUntilListOfElementsIsVisible(By.xpath(xpath), driver);
        List<WebElement> resultsDisplayed = driver.findElements(By.xpath(xpath));
        WebElement firstMatchResult = resultsDisplayed.get(0);
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath(xpath), driver);
        try {
            firstMatchResult.click();
        } catch (StaleElementReferenceException e) {
            e.printStackTrace();
        }
    }


    public void selectFirstFromListWithActions(String xpath) {
        SeleniumUtils.waitUntilListOfElementsIsVisible(By.xpath(xpath), driver);
        List<WebElement> resultsDisplayed = waitForPresenceOfAllElements(driver, 30, By.xpath(xpath));
        WebElement firstMatchResult = resultsDisplayed.get(0);
        SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath(xpath), driver);
        try {
            actions.moveToElement(firstMatchResult);
            firstMatchResult.click();
        }catch (ElementNotInteractableException e){
            e.printStackTrace();
        }
    }


    public void addFirstProductToBaketWithActions(String xpath) {
        try {
            List<WebElement> resultsDisplayed = waitForPresenceOfAllElements(driver, 30, By.xpath(xpath));
            WebElement firstMatchResult = resultsDisplayed.get(0);
            actions.moveToElement(firstMatchResult).click().build().perform();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    public boolean isElementPresentBy(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isElementPresentByWithWait(By by) {
        try {
            Thread.sleep(1500);
            driver.findElement(by);
            return true;
        } catch (InterruptedException | NoSuchElementException e) {
            return false;
        }
    }

    public boolean isElementPresentByXpath(String xpath) {
        try {
            driver.findElement(By.xpath(xpath));
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void sendKeysToInput(String xpath, String searchedItem) {
        WebElement searchInput = SeleniumUtils.waitUntilElementIsVisibleAndClickeble(By.xpath(xpath), driver);
        searchInput.clear();
        searchInput.sendKeys(searchedItem);
    }

    public void sendKeysToInputByWebElement(WebElement element, String keys) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        element.sendKeys(keys);
    }

    public void sendKeysToInputByWebElementEnter(WebElement element, String keys) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        element.sendKeys(keys);
        element.sendKeys(Keys.ENTER);
    }

    public void sendKeysToSearchInputEnter(String xpath, String searchedItem) {
        WebElement searchInput = waitForElementToBeClickable(driver, 30, By.xpath(xpath));
        searchInput.click();
        searchInput.sendKeys(searchedItem);
        searchInput.sendKeys(Keys.ENTER);
    }


    public void waitForElementToBeClickableClick(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public void clickWithWait(By by) {
        SeleniumUtils.waitUntilIdentifiedElementIsVisibleAndClickeble(by, driver);
        WebElement element = driver.findElement(by);
        try {
            element.click();
        }catch (ElementClickInterceptedException e){
            e.printStackTrace();
        }

    }


}